#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Mandy@azip'
SITENAME = 'AS IIS summer internship 2020 noobie scribbles'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('Wikidata', 'https://www.wikidata.org/wiki/Wikidata:Main_Page'),
         ('Wikidata Query Service', 'https://query.wikidata.org/'),
         ('scholia/toolforge', 'https://scholia.toolforge.org/'),
         ('PetScan', 'https://petscan.wmflabs.org/'),
         ('OpenStreetMap', 'https://www.openstreetmap.org/'))

# Social widget
SOCIAL = (('website liftoff', 'https://azip.gitlab.io/website-liftoff'),
          ('MyJekyll', 'https://azip.gitlab.io/'))

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
