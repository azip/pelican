Title: Wikidata Query Service
Date: 2020-08-31
Category: Query
Tags: Query, Wikidata
Slug: wikidata-query-service
Author: Mandy  

## Wikidata's query language: SPARQL  
Wikidata is a RDF (Resource Description Framework) database and SPARQL is a query language for searching and manipulating RDF databases. Run your Wikidata query at [Wikidata Query Service](https://query.wikidata.org/).  

---
## Penghu's religious sites  
The following are some query results when attempting to answer questions about the documented places of worship and temples in Penghu county.  

First of all, we have to know the instance types and subclass hierarchy applicable for temples and religious sites:

>...... (更上級的分類)  
>**&#8657;**  
>[宗教場所  place of worship Q1370598](https://www.wikidata.org/wiki/Q1370598)  
>**&#8657;**  
>[宗教建築  religious building Q24398318](https://www.wikidata.org/wiki/Q24398318)   
>**&#8657;**  
>[神廟     temple          Q44539](https://www.wikidata.org/wiki/Q44539)   
>**&#8657;**  
>[寺廟   Joss house    Q29043583 ](https://www.wikidata.org/wiki/Q29043583)  
>**&#8657;**  
>[中華廟宇 Chinese temple Q2680845](https://www.wikidata.org/wiki/Q2680845)  
>**&#8657;**   
>[媽祖廟 temples of Mazu Q7245816](https://www.wikidata.org/wiki/Q7245816)；[土地廟 temples of >Tudigongs Q10927673](https://www.wikidata.org/wiki/Q10927673)；  
>[地藏庵 temples of Dìzàng Q10928417](https://www.wikidata.org/wiki/Q10928417)；[文昌廟 >Wenchang temple Q11079898](https://www.wikidata.org/wiki/Q11079898)；  
>[王爺廟 Wang Yeh temple Q15914744 ](https://www.wikidata.org/wiki/Q15914744)；[城隍廟temples >of City God Q21683788](https://www.wikidata.org/wiki/Q21683788)；  
>[土地公廟 Q81470346](https://www.wikidata.org/wiki/Q81470346)；[西王母廟 temples of Queen >Mother of the West Q81776104](https://www.wikidata.org/wiki/Q81776104)

****&#8657;**** represents the property of **上級分類 subclass of (P279)**, that means e.g. 中華廟宇 is a subclass of 寺廟.  

## Query 有什麼物件的上級分類或/及上上級分類是宗教場所？  

try: [https://w.wiki/awG](https://w.wiki/awG)  

```sparql
#上級分類是宗教場所的物件
#defaultView:Graph
SELECT ?class ?classLabel ?subclass ?subclassLabel ?subclassDescription
WHERE {
  VALUES ?class {
    wd:Q1370598
  }
  ?subclass wdt:P279/wdt:P279* ?class. #尋找上級分類或及其上上級分類是宗教場所的Q
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],zh-tw, zh, zh-hant, en, de, nl, ru, pl". }
}
LIMIT 1000
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E6%98%AF%E5%AE%97%E6%95%99%E5%A0%B4%E6%89%80%E7%9A%84%E7%89%A9%E4%BB%B6%0A%23defaultView%3AGraph%0ASELECT%20%3Fclass%20%3FclassLabel%20%3Fsubclass%20%3FsubclassLabel%20%3FsubclassDescription%0AWHERE%20%7B%0A%20%20VALUES%20%3Fclass%20%7B%0A%20%20%20%20wd%3AQ1370598%0A%20%20%7D%0A%20%20%3Fsubclass%20wdt%3AP279%2Fwdt%3AP279%2a%20%3Fclass.%20%23%E5%B0%8B%E6%89%BE%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E6%88%96%E5%8F%8A%E5%85%B6%E4%B8%8A%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E6%98%AF%E5%AE%97%E6%95%99%E5%A0%B4%E6%89%80%E7%9A%84Q%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Czh-tw%2C%20zh%2C%20zh-hant%2C%20en%2C%20de%2C%20nl%2C%20ru%2C%20pl%22.%20%7D%0A%7D%0ALIMIT%201000" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/awJ](https://w.wiki/awJ)  
Comment: 設定default view 是 Graph，可以更清楚看到物件與中心「宗教場所」的關聯度。  

---
# 關於澎湖  
**在Wikidata 現有的資料裏，**

## Query A : 澎湖縣的行政分區 

try: [https://w.wiki/asT](https://w.wiki/asT) 

```sparql
#澎湖縣的行政分區
SELECT DISTINCT ?country ?countryLabel ?county ?countyLabel ?upperlocality ?upperlocalityLabel ?lowerlocality ?lowerlocalityLabel ?geoCoord
WHERE {
  VALUES ?county {wd:Q198525} #澎湖縣(二級行政區) 
  ?county wdt:P150 ?upperlocality. #澎湖縣-包含的行政領土實體-縣轄市/鄉(三級行政區)
  OPTIONAL {?county wdt:P17  ?country. } #澎湖縣-所屬的主權國
  OPTIONAL {?lowerlocality wdt:P131 ?upperlocality; #?lowerlocality代表里/村(四級行政區)-其所在領土實體是-縣轄市/鄉(三級行政區) 並
                           wdt:P31  wd:Q7930614. } # 性質爲-村里(中華民國行政規劃)
  OPTIONAL {?lowerlocality wdt:P625 ?geoCoord. } #村里-地理座標(點)
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E8%A1%8C%E6%94%BF%E5%88%86%E5%8D%80%0ASELECT%20DISTINCT%20%3Fcountry%20%3FcountryLabel%20%3Fcounty%20%3FcountyLabel%20%3Fupperlocality%20%3FupperlocalityLabel%20%3Flowerlocality%20%3FlowerlocalityLabel%20%3FgeoCoord%0AWHERE%20%7B%0A%20%20VALUES%20%3Fcounty%20%7Bwd%3AQ198525%7D%20%23%E6%BE%8E%E6%B9%96%E7%B8%A3%28%E4%BA%8C%E7%B4%9A%E8%A1%8C%E6%94%BF%E5%8D%80%29%20%0A%20%20%3Fcounty%20wdt%3AP150%20%3Fupperlocality.%20%23%E6%BE%8E%E6%B9%96%E7%B8%A3-%E5%8C%85%E5%90%AB%E7%9A%84%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94-%E7%B8%A3%E8%BD%84%E5%B8%82%2F%E9%84%89%28%E4%B8%89%E7%B4%9A%E8%A1%8C%E6%94%BF%E5%8D%80%29%0A%20%20OPTIONAL%20%7B%3Fcounty%20wdt%3AP17%20%20%3Fcountry.%20%7D%20%23%E6%BE%8E%E6%B9%96%E7%B8%A3-%E6%89%80%E5%B1%AC%E7%9A%84%E4%B8%BB%E6%AC%8A%E5%9C%8B%0A%20%20OPTIONAL%20%7B%3Flowerlocality%20wdt%3AP131%20%3Fupperlocality%3B%20%23%3Flowerlocality%E4%BB%A3%E8%A1%A8%E9%87%8C%2F%E6%9D%91%28%E5%9B%9B%E7%B4%9A%E8%A1%8C%E6%94%BF%E5%8D%80%29-%E5%85%B6%E6%89%80%E5%9C%A8%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E6%98%AF-%E7%B8%A3%E8%BD%84%E5%B8%82%2F%E9%84%89%28%E4%B8%89%E7%B4%9A%E8%A1%8C%E6%94%BF%E5%8D%80%29%20%E4%B8%A6%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20wdt%3AP31%20%20wd%3AQ7930614.%20%7D%20%23%20%E6%80%A7%E8%B3%AA%E7%88%B2-%E6%9D%91%E9%87%8C%28%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B%E8%A1%8C%E6%94%BF%E8%A6%8F%E5%8A%83%29%0A%20%20OPTIONAL%20%7B%3Flowerlocality%20wdt%3AP625%20%3FgeoCoord.%20%7D%20%23%E6%9D%91%E9%87%8C-%E5%9C%B0%E7%90%86%E5%BA%A7%E6%A8%99%28%E9%BB%9E%29%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/asS](https://w.wiki/asS)  

Comment: 這個顯示了澎湖縣的一市五鄉及其下轄里、村。因爲```OPTIONAL```要求村里的地理座標，table mode 也可以換地圖模式來檢視。  

## Query B : 澎湖縣的物件

try: [https://w.wiki/awU](https://w.wiki/awU) 

```sparql
#wikidata有記錄的澎湖縣的物件
SELECT DISTINCT ?upperLocateLabel ?entity ?entityLabel (GROUP_CONCAT(DISTINCT ?typeLabel; SEPARATOR = ", ") AS ?typeLabel_list) ?entityDescription ?geo  ?osmRelation
WHERE {
  ?entity wdt:P131/(wdt:P131*) wd:Q198525 #物件在澎湖縣的行政領土實體範圍內
  OPTIONAL {?entity wdt:P31 ?type. }  #物件的性質
  OPTIONAL { ?entity wdt:P131 ?upperLocate. } #物件所屬行政領土實體
  OPTIONAL { ?entity wdt:P625 ?geo. } #物件的地理座標
  OPTIONAL  { ?entity wdt:P402 ?osmRelation}  #物件的OSM關聯識別碼
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en".
    ?type rdfs:label ?typeLabel.
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
GROUP BY ?upperLocateLabel ?entity ?entityLabel  ?entityDescription ?geo ?osmRelation
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23wikidata%E6%9C%89%E8%A8%98%E9%8C%84%E7%9A%84%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E7%89%A9%E4%BB%B6%0ASELECT%20DISTINCT%20%3FupperLocateLabel%20%3Fentity%20%3FentityLabel%20%28GROUP_CONCAT%28DISTINCT%20%3FtypeLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FtypeLabel_list%29%20%3FentityDescription%20%3Fgeo%20%20%3FosmRelation%0AWHERE%20%7B%0A%20%20%3Fentity%20wdt%3AP131%2F%28wdt%3AP131%2a%29%20wd%3AQ198525%20%23%E7%89%A9%E4%BB%B6%E5%9C%A8%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E7%AF%84%E5%9C%8D%E5%85%A7%0A%20%20OPTIONAL%20%7B%3Fentity%20wdt%3AP31%20%3Ftype.%20%7D%20%20%23%E7%89%A9%E4%BB%B6%E7%9A%84%E6%80%A7%E8%B3%AA%0A%20%20OPTIONAL%20%7B%20%3Fentity%20wdt%3AP131%20%3FupperLocate.%20%7D%20%23%E7%89%A9%E4%BB%B6%E6%89%80%E5%B1%AC%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%0A%20%20OPTIONAL%20%7B%20%3Fentity%20wdt%3AP625%20%3Fgeo.%20%7D%20%23%E7%89%A9%E4%BB%B6%E7%9A%84%E5%9C%B0%E7%90%86%E5%BA%A7%E6%A8%99%0A%20%20OPTIONAL%20%20%7B%20%3Fentity%20wdt%3AP402%20%3FosmRelation%7D%20%20%23%E7%89%A9%E4%BB%B6%E7%9A%84OSM%E9%97%9C%E8%81%AF%E8%AD%98%E5%88%A5%E7%A2%BC%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%0A%20%20%20%20%3Ftype%20rdfs%3Alabel%20%3FtypeLabel.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D%0AGROUP%20BY%20%3FupperLocateLabel%20%3Fentity%20%3FentityLabel%20%20%3FentityDescription%20%3Fgeo%20%3FosmRelation%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/awV](https://w.wiki/awV)  

Comment: 所有在wikidata有標其地點在澎湖縣的物件都出現啦，510個結果，可以依照物件的性質類型、名稱、所屬行政領土實體來排列，看想要的資料。從這個query可看到，中華廟宇(有幾個是列表)86個，佛寺4個，土地公廟3個，媽祖廟(instance單獨性質沒有加上中華廟宇)3個，王爺廟(instance單獨性質沒有加上中華廟宇)10個，祠(instance單獨性質沒有加上中華廟宇)2個，共108個。每次query都思索一下，還有沒有漏網之魚或者重覆的結果呢？......

---

## Query 1：有多少個澎湖縣的宗教場所？  

```sparql
SELECT DISTINCT ?worshipPlace ?worshipPlaceLabel ?worshipPlaceDescription ?type ?typeLabel ?countryLabel ?locality ?localityLabel ?postcode ?address ?geo (year(?date) as ?year) ?commons ?commonsIn  
WHERE {  
  VALUES ?type {wd:Q1370598}  
  ?worshipPlace (wdt:P31/(wdt:P279*)) ?type; #必-性質或其上級分類爲宗教場所並顯示在type欄  
    (wdt:P131/(wdt:P131*)) wd:Q198525. #必-所在行政領土實體 - 澎湖縣  
  OPTIONAL { ?worshipPlace wdt:P17 ?country. } #國家  
  OPTIONAL { ?worshipPlace wdt:P131 ?locality. } #所在行政領土實體  
  OPTIONAL { ?worshipPlace wdt:P281 ?postcode. } #郵遞區號  
  OPTIONAL { ?worshipPlace wdt:P6375 ?address. } #地址  
  OPTIONAL { ?worshipPlace wdt:P625 ?geo. }      #地理座標  
  OPTIONAL { ?worshipPlace wdt:P571 ?date. }     #成立時間年份  
  OPTIONAL { ?worshipPlace wdt:P373 ?commons. }  #Commons category 共享資源分類  
  OPTIONAL { ?worshipPlace wdt:P7561 ?commonsIn. } #Wiki commons interior 用於某項內部的共享資源分類  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }  
}  
```

<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%E5%9C%A8%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E5%AE%97%E6%95%99%E5%A0%B4%E6%89%80%0ASELECT%20DISTINCT%20%3FworshipPlace%20%3FworshipPlaceLabel%20%3FworshipPlaceDescription%20%3Ftype%20%3FtypeLabel%20%3FcountryLabel%20%3Flocality%20%3FlocalityLabel%20%3Fpostcode%20%3Faddress%20%3Fgeo%20%28year%28%3Fdate%29%20as%20%3Fyear%29%20%3Fcommons%20%3FcommonsIn%20%0AWHERE%20%7B%0A%20%20VALUES%20%3Ftype%20%7Bwd%3AQ1370598%7D%20%0A%20%20%3FworshipPlace%20%28wdt%3AP31%2F%28wdt%3AP279%2a%29%29%20%3Ftype%3B%20%23%E5%BF%85-%E6%80%A7%E8%B3%AA%E6%88%96%E5%85%B6%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E7%88%B2%E5%AE%97%E6%95%99%E5%A0%B4%E6%89%80%E4%B8%A6%E9%A1%AF%E7%A4%BA%E5%9C%A8type%E6%AC%84%0A%20%20%20%20%28wdt%3AP131%2F%28wdt%3AP131%2a%29%29%20wd%3AQ198525.%20%23%E5%BF%85-%E6%89%80%E5%9C%A8%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%20-%20%E6%BE%8E%E6%B9%96%E7%B8%A3%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP17%20%3Fcountry.%20%7D%20%23%E5%9C%8B%E5%AE%B6%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP131%20%3Flocality.%20%7D%20%23%E6%89%80%E5%9C%A8%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP281%20%3Fpostcode.%20%7D%20%23%E9%83%B5%E9%81%9E%E5%8D%80%E8%99%9F%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP6375%20%3Faddress.%20%7D%20%23%E5%9C%B0%E5%9D%80%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP625%20%3Fgeo.%20%7D%20%20%20%20%20%20%23%E5%9C%B0%E7%90%86%E5%BA%A7%E6%A8%99%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP571%20%3Fdate.%20%7D%20%20%20%20%20%23%E6%88%90%E7%AB%8B%E6%99%82%E9%96%93%E5%B9%B4%E4%BB%BD%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP373%20%3Fcommons.%20%7D%20%20%23Commons%20category%20%E5%85%B1%E4%BA%AB%E8%B3%87%E6%BA%90%E5%88%86%E9%A1%9E%0A%20%20OPTIONAL%20%7B%20%3FworshipPlace%20wdt%3AP7561%20%3FcommonsIn.%20%7D%20%23Wiki%20commons%20interior%20%E7%94%A8%E6%96%BC%E6%9F%90%E9%A0%85%E5%85%A7%E9%83%A8%E7%9A%84%E5%85%B1%E4%BA%AB%E8%B3%87%E6%BA%90%E5%88%86%E9%A1%9E%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

Comment: 共有108個結果，其中兩個結果是 澎湖縣馬公市寺廟列表 （Q63164276）和 澎湖廟宇籤詩列表 （Q64974109），不是宗教場所的entity, 所以實際上是有106個澎湖縣的宗教場所已經在Wikidata建檔，當中大部分是佛道民間信仰廟宇會堂，只有兩個基督教和天主教的教堂。接下來我們試試用其他相關的instance作爲條件，看看結果如何。  

## Query 2：有多少個澎湖縣的宗教建築？  

try: []() 

```sparql

```  
<

另開網頁看結果：[]()  

Comment: 

## Query 3：有多少個澎湖縣的神廟？

try: [https://w.wiki/axA](https://w.wiki/axA) 

```sparql
#在澎湖縣的神廟
#性質或其上級分類爲神廟的+所在行政領土實體或其行政領土實體的上級分類爲澎湖縣=must requirements, 若有請顯示：行政領土實體、地理座標、成立日期
#DISTINCT 不重覆 兩個西吉宮
#defaultView:Map
SELECT DISTINCT ?temple ?templeLabel ?locality ?localityLabel ?geo ?inception WHERE {
  ?temple (wdt:P31/(wdt:P279*)) wd:Q44539;
    (wdt:P131/(wdt:P131*)) wd:Q198525.
  OPTIONAL { ?temple wdt:P131 ?locality. }
  OPTIONAL { ?temple wdt:P625 ?geo. }
  OPTIONAL { ?temple wdt:P571 ?inception. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%E5%9C%A8%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E7%A5%9E%E5%BB%9F%0A%23%E6%80%A7%E8%B3%AA%E6%88%96%E5%85%B6%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E7%88%B2%E7%A5%9E%E5%BB%9F%E7%9A%84%2B%E6%89%80%E5%9C%A8%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E6%88%96%E5%85%B6%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E7%9A%84%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E7%88%B2%E6%BE%8E%E6%B9%96%E7%B8%A3%3Dmust%20requirements%2C%20%E8%8B%A5%E6%9C%89%E8%AB%8B%E9%A1%AF%E7%A4%BA%EF%BC%9A%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E3%80%81%E5%9C%B0%E7%90%86%E5%BA%A7%E6%A8%99%E3%80%81%E6%88%90%E7%AB%8B%E6%97%A5%E6%9C%9F%0A%23DISTINCT%20%E4%B8%8D%E9%87%8D%E8%A6%86%20%E5%85%A9%E5%80%8B%E8%A5%BF%E5%90%89%E5%AE%AE%0A%23defaultView%3AMap%0ASELECT%20DISTINCT%20%3Ftemple%20%3FtempleLabel%20%3Flocality%20%3FlocalityLabel%20%3Fgeo%20%3Finception%20WHERE%20%7B%0A%20%20%3Ftemple%20%28wdt%3AP31%2F%28wdt%3AP279%2a%29%29%20wd%3AQ44539%3B%0A%20%20%20%20%28wdt%3AP131%2F%28wdt%3AP131%2a%29%29%20wd%3AQ198525.%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP131%20%3Flocality.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP625%20%3Fgeo.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP571%20%3Finception.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/axC](https://w.wiki/axC)  

Comment: 共106個。這個設定default view 是map.


## Query 4：有多少個澎湖縣的寺廟？
1.最簡單查詢item and geo coordinate
try: [https://w.wiki/awy](https://w.wiki/awy) 

```sparql
SELECT DISTINCT ?item ?itemLabel ?geo
WHERE{
  ?item wdt:P31/(wdt:P279*) wd:Q29043583;
        wdt:P131/(wdt:P131*) wd:Q198525.
  OPTIONAL {?item wdt:P625 ?geo .}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#SELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20%3Fgeo%0AWHERE%7B%0A%20%20%3Fitem%20wdt%3AP31%2F%28wdt%3AP279%2a%29%20wd%3AQ29043583%3B%0A%20%20%20%20%20%20%20%20wdt%3AP131%2F%28wdt%3AP131%2a%29%20wd%3AQ198525.%0A%20%20OPTIONAL%20%7B%3Fitem%20wdt%3AP625%20%3Fgeo%20.%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/aw$](https://w.wiki/aw$)  

Comment: 有100個，這個應該是最基本廟宇的數目，除了一個列表，所以就是說有99個廟；現有的完整澎湖的廟宇祠堂有約250個，所以還有150多個的廟宇的資料要建。




複雜版query
try: [https://w.wiki/awv](https://w.wiki/awv) 

```sparql
#在澎湖縣的神廟
#性質或其上級分類爲神廟的+所在行政領土實體或其行政領土實體的上級分類爲澎湖縣=must requirements, 若有請顯示：”澎湖廟宇“結果的行政領土實體、地理座標、照片、成立日期
#DISTINCT 不重覆; 因爲選擇顯示image及locality，而有temple的image多於一張/locality多於一個，所以templeLabel會有e.g.兩個西吉宮(P131個)，兩個媽宮風神廟(2張照片)

SELECT DISTINCT ?temple ?templeLabel ?locality ?localityLabel ?geo ?image ?inception
WHERE {
  ?temple (wdt:P31/(wdt:P279*)) wd:Q44539;
    (wdt:P131/(wdt:P131*)) wd:Q198525.
  OPTIONAL { ?temple wdt:P131 ?locality. }
  OPTIONAL { ?temple wdt:P625 ?geo. }
  OPTIONAL { ?temple wdt:P18  ?image. }
  OPTIONAL { ?temple wdt:P571 ?inception}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%E5%9C%A8%E6%BE%8E%E6%B9%96%E7%B8%A3%E7%9A%84%E7%A5%9E%E5%BB%9F%0A%23%E6%80%A7%E8%B3%AA%E6%88%96%E5%85%B6%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E7%88%B2%E7%A5%9E%E5%BB%9F%E7%9A%84%2B%E6%89%80%E5%9C%A8%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E6%88%96%E5%85%B6%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E7%9A%84%E4%B8%8A%E7%B4%9A%E5%88%86%E9%A1%9E%E7%88%B2%E6%BE%8E%E6%B9%96%E7%B8%A3%3Dmust%20requirements%2C%20%E8%8B%A5%E6%9C%89%E8%AB%8B%E9%A1%AF%E7%A4%BA%EF%BC%9A%E2%80%9D%E6%BE%8E%E6%B9%96%E5%BB%9F%E5%AE%87%E2%80%9C%E7%B5%90%E6%9E%9C%E7%9A%84%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%E3%80%81%E5%9C%B0%E7%90%86%E5%BA%A7%E6%A8%99%E3%80%81%E7%85%A7%E7%89%87%E3%80%81%E6%88%90%E7%AB%8B%E6%97%A5%E6%9C%9F%0A%23DISTINCT%20%E4%B8%8D%E9%87%8D%E8%A6%86%3B%20%E5%9B%A0%E7%88%B2%E9%81%B8%E6%93%87%E9%A1%AF%E7%A4%BAimage%E5%8F%8Alocality%EF%BC%8C%E8%80%8C%E6%9C%89temple%E7%9A%84image%E5%A4%9A%E6%96%BC%E4%B8%80%E5%BC%B5%2Flocality%E5%A4%9A%E6%96%BC%E4%B8%80%E5%80%8B%EF%BC%8C%E6%89%80%E4%BB%A5templeLabel%E6%9C%83%E6%9C%89e.g.%E5%85%A9%E5%80%8B%E8%A5%BF%E5%90%89%E5%AE%AE%28P131%E5%80%8B%29%EF%BC%8C%E5%85%A9%E5%80%8B%E5%AA%BD%E5%AE%AE%E9%A2%A8%E7%A5%9E%E5%BB%9F%282%E5%BC%B5%E7%85%A7%E7%89%87%29%0A%0ASELECT%20DISTINCT%20%3Ftemple%20%3FtempleLabel%20%3Flocality%20%3FlocalityLabel%20%3Fgeo%20%3Fimage%20%3Finception%0AWHERE%20%7B%0A%20%20%3Ftemple%20%28wdt%3AP31%2F%28wdt%3AP279%2a%29%29%20wd%3AQ44539%3B%0A%20%20%20%20%28wdt%3AP131%2F%28wdt%3AP131%2a%29%29%20wd%3AQ198525.%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP131%20%3Flocality.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP625%20%3Fgeo.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP18%20%20%3Fimage.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Ftemple%20wdt%3AP571%20%3Finception%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/aww](https://w.wiki/aww)  

Comment: 有107個結果，但是要用label-list group 起來才不會有重覆count

## Query 5：有多少個澎湖縣的中華廟宇？

try: [https://w.wiki/ax4](https://w.wiki/ax4) 

```sparql
SELECT ?item ?itemLabel ?locality ?localityLabel ?geo ?typeLabel
WHERE {
  ?item (wdt:P31/(wdt:P279*)) wd:Q24398318;
    (wdt:P131/(wdt:P131*)) wd:Q198525.
  OPTIONAL {?item wdt:P31 ?type. }
  OPTIONAL { ?item wdt:P131 ?locality. }
  OPTIONAL { ?item wdt:P625 ?geo. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw,zh,zh-hant,en". }
}
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#SELECT%20%3Fitem%20%3FitemLabel%20%3Flocality%20%3FlocalityLabel%20%3Fgeo%20%3FtypeLabel%0AWHERE%20%7B%0A%20%20%3Fitem%20%28wdt%3AP31%2F%28wdt%3AP279%2a%29%29%20wd%3AQ24398318%3B%0A%20%20%20%20%28wdt%3AP131%2F%28wdt%3AP131%2a%29%29%20wd%3AQ198525.%0A%20%20OPTIONAL%20%7B%3Fitem%20wdt%3AP31%20%3Ftype.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP131%20%3Flocality.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP625%20%3Fgeo.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2Czh%2Czh-hant%2Cen%22.%20%7D%0A%7D" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/ax5](https://w.wiki/ax5)  

Comment: 130個結果，但是有重覆的，因爲同一個entity 的instance 有兩個以上，所以可以用group BY 來改進

## Query 6：中華民國(Q865)有是宗教建築又/或是宗教場所？

try: [https://w.wiki/ax7](https://w.wiki/ax7) 

```sparql
# succesfully grouped by 
SELECT ?item ?itemLabel ?itemDescription (GROUP_CONCAT(DISTINCT ?countryLabel; SEPARATOR = ", ") AS ?countryLabel_list) (GROUP_CONCAT(DISTINCT ?cityLabel; SEPARATOR = ", ") AS ?cityLabel_list) (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR = ", ") AS ?altLabel_list) (GROUP_CONCAT(DISTINCT ?typeLabel; SEPARATOR = ", ") AS ?typeLabel_list)
WHERE {
  VALUES ?type {wd:Q24398318 wd:Q1370598}
  ?item wdt:P31/(wdt:P279*) ?type;
        wdt:P17 wd:Q865.
  OPTIONAL { ?item wdt:P17 ?country. }
  OPTIONAL { ?item wdt:P131 ?city. }
  OPTIONAL { ?item skos:altLabel ?altLabel. FILTER(LANG(?altLabel) IN ("zh-tw", "zh", "zh-hant", "en")) }
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en".
    ?country rdfs:label ?countryLabel.
    ?city rdfs:label ?cityLabel.
    ?type rdfs:label ?typeLabel.
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en". }
}
GROUP BY ?item ?itemLabel ?itemDescription ?altLabel_list

```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%23%20succesfully%20grouped%20by%20%0ASELECT%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%28GROUP_CONCAT%28DISTINCT%20%3FcountryLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcountryLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FcityLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcityLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FaltLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FaltLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FtypeLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FtypeLabel_list%29%0AWHERE%20%7B%0A%20%20VALUES%20%3Ftype%20%7Bwd%3AQ24398318%20wd%3AQ1370598%7D%0A%20%20%3Fitem%20wdt%3AP31%2F%28wdt%3AP279%2a%29%20%3Ftype%3B%0A%20%20%20%20%20%20%20%20wdt%3AP17%20wd%3AQ865.%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP17%20%3Fcountry.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP131%20%3Fcity.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20skos%3AaltLabel%20%3FaltLabel.%20FILTER%28LANG%28%3FaltLabel%29%20IN%20%28%22zh-tw%22%2C%20%22zh%22%2C%20%22zh-hant%22%2C%20%22en%22%29%29%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%0A%20%20%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel.%0A%20%20%20%20%3Fcity%20rdfs%3Alabel%20%3FcityLabel.%0A%20%20%20%20%3Ftype%20rdfs%3Alabel%20%3FtypeLabel.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%20%7D%0A%7D%0AGROUP%20BY%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3FaltLabel_list%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>
另開網頁看結果：[https://w.wiki/ax9](https://w.wiki/ax9)  

Comment: 1018g個結果


## Query 7：中華民國臺灣(Q865)有多少個宗教場所或/及寺廟?

try: [https://w.wiki/awp](https://w.wiki/awp) 

```sparql

SELECT ?item ?itemLabel ?itemDescription (GROUP_CONCAT(DISTINCT ?countryLabel; SEPARATOR = ", ") AS ?countryLabel_list) (GROUP_CONCAT(DISTINCT ?cityLabel; SEPARATOR = ", ") AS ?cityLabel_list) (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR = ", ") AS ?altLabel_list) (GROUP_CONCAT(DISTINCT ?typeLabel; SEPARATOR = ", ") AS ?typeLabel_list)
WHERE {
  VALUES ?type {wd:Q1370598 wd:Q29043583} #條件寺廟或及宗教場所
  ?item wdt:P31/(wdt:P279*) ?type;
        wdt:P17 wd:Q865. #條件中華民國(臺灣)
  OPTIONAL { ?item wdt:P17 ?country. } #中華民國 
  OPTIONAL { ?item wdt:P131 ?city. } #所屬行政領土實體
  OPTIONAL { ?item skos:altLabel ?altLabel. FILTER(LANG(?altLabel) IN ("zh-tw", "zh", "zh-hant", "en")) } #別名
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en".
    ?country rdfs:label ?countryLabel.
    ?city rdfs:label ?cityLabel.
    ?type rdfs:label ?typeLabel.
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en". }
}
GROUP BY ?item ?itemLabel ?itemDescription ?altLabel_list

```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#%0ASELECT%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%28GROUP_CONCAT%28DISTINCT%20%3FcountryLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcountryLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FcityLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcityLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FaltLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FaltLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FtypeLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FtypeLabel_list%29%0AWHERE%20%7B%0A%20%20VALUES%20%3Ftype%20%7Bwd%3AQ1370598%20wd%3AQ29043583%7D%20%23%E6%A2%9D%E4%BB%B6%E5%AF%BA%E5%BB%9F%E6%88%96%E5%8F%8A%E5%AE%97%E6%95%99%E5%A0%B4%E6%89%80%0A%20%20%3Fitem%20wdt%3AP31%2F%28wdt%3AP279%2a%29%20%3Ftype%3B%0A%20%20%20%20%20%20%20%20wdt%3AP17%20wd%3AQ865.%20%23%E6%A2%9D%E4%BB%B6%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B%28%E8%87%BA%E7%81%A3%29%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP17%20%3Fcountry.%20%7D%20%23%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B%20%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP131%20%3Fcity.%20%7D%20%23%E6%89%80%E5%B1%AC%E8%A1%8C%E6%94%BF%E9%A0%98%E5%9C%9F%E5%AF%A6%E9%AB%94%0A%20%20OPTIONAL%20%7B%20%3Fitem%20skos%3AaltLabel%20%3FaltLabel.%20FILTER%28LANG%28%3FaltLabel%29%20IN%20%28%22zh-tw%22%2C%20%22zh%22%2C%20%22zh-hant%22%2C%20%22en%22%29%29%20%7D%20%23%E5%88%A5%E5%90%8D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%0A%20%20%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel.%0A%20%20%20%20%3Fcity%20rdfs%3Alabel%20%3FcityLabel.%0A%20%20%20%20%3Ftype%20rdfs%3Alabel%20%3FtypeLabel.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%20%7D%0A%7D%0AGROUP%20BY%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3FaltLabel_list%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/aws](https://w.wiki/aws)  

Comment: 

每一個item 有建Wikipedia維基百科的頁嗎？

try: [https://w.wiki/aSs](https://w.wiki/aSs) 

```sparql
SELECT ?item ?itemLabel ?itemDescription ?sitelink (GROUP_CONCAT(DISTINCT ?countryLabel; SEPARATOR = ", ") AS ?countryLabel_list) (GROUP_CONCAT(DISTINCT ?cityLabel; SEPARATOR = ", ") AS ?cityLabel_list) (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR = ", ") AS ?altLabel_list) (GROUP_CONCAT(DISTINCT ?typeLabel; SEPARATOR = ", ") AS ?typeLabel_list)
WHERE {
  VALUES ?type {wd:Q24398318 wd:Q1370598}
  ?item wdt:P31/(wdt:P279*) ?type;
        wdt:P17 wd:Q865.
  
  OPTIONAL { ?item wdt:P17 ?country. }
  OPTIONAL { ?item wdt:P131 ?city. }
  OPTIONAL { ?item skos:altLabel ?altLabel. FILTER(LANG(?altLabel) IN ("zh-tw", "zh", "zh-hant", "en")) }
  OPTIONAL { 
    ?sitelink schema:about ?item; 
              schema:isPartOf <https://zh.wikipedia.org/>.
  }
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en".
    ?country rdfs:label ?countryLabel.
    ?city rdfs:label ?cityLabel.
    ?type rdfs:label ?typeLabel.
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "zh-tw, zh, zh-hant, en". }
}
GROUP BY ?item ?itemLabel ?itemDescription ?altLabel_list ?wikipedia ?sitelink
```  
<iframe style="width: 40vw; height: 30vh; border: none;" src="https://query.wikidata.org/embed.html#SELECT%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3Fsitelink%20%28GROUP_CONCAT%28DISTINCT%20%3FcountryLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcountryLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FcityLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FcityLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FaltLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FaltLabel_list%29%20%28GROUP_CONCAT%28DISTINCT%20%3FtypeLabel%3B%20SEPARATOR%20%3D%20%22%2C%20%22%29%20AS%20%3FtypeLabel_list%29%0AWHERE%20%7B%0A%20%20VALUES%20%3Ftype%20%7Bwd%3AQ24398318%20wd%3AQ1370598%7D%0A%20%20%3Fitem%20wdt%3AP31%2F%28wdt%3AP279%2a%29%20%3Ftype%3B%0A%20%20%20%20%20%20%20%20wdt%3AP17%20wd%3AQ865.%0A%20%20%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP17%20%3Fcountry.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP131%20%3Fcity.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20skos%3AaltLabel%20%3FaltLabel.%20FILTER%28LANG%28%3FaltLabel%29%20IN%20%28%22zh-tw%22%2C%20%22zh%22%2C%20%22zh-hant%22%2C%20%22en%22%29%29%20%7D%0A%20%20OPTIONAL%20%7B%20%0A%20%20%20%20%3Fsitelink%20schema%3Aabout%20%3Fitem%3B%20%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fzh.wikipedia.org%2F%3E.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%0A%20%20%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel.%0A%20%20%20%20%3Fcity%20rdfs%3Alabel%20%3FcityLabel.%0A%20%20%20%20%3Ftype%20rdfs%3Alabel%20%3FtypeLabel.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22zh-tw%2C%20zh%2C%20zh-hant%2C%20en%22.%20%7D%0A%7D%0AGROUP%20BY%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3FaltLabel_list%20%3Fwikipedia%20%3Fsitelink%0A" referrerpolicy="origin" sandbox="allow-scripts allow-same-origin allow-popups" ></iframe>

另開網頁看結果：[https://w.wiki/awu](https://w.wiki/awu)  

Comment: 
